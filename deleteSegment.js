var request = require('request-promise');
var hashedKey = Buffer.from('91ff81c6f743f0313033:076269577f9d1d824eed').toString("base64")
var auth = `Basic ${hashedKey}`;
const yargs = require('yargs');

const argv = yargs.options({
  s: {
    demand: true,
    alias: 'segment',
    describe: 'The id of the segment you want to delete',
    string: true
  },
  l: {
    demand: false,
    alias: 'limit',
    describe: 'The limit of users you want to delete',
    string: true
  }
}).help().alias('help', 'h').argv;

var segmentId = argv.s;
if (argv.l) {
  var limit = argv.l;
} else {
  var limit = "30";
}

function DeleteData() {
  request({
    "method": "GET",
    "uri": `https://beta-api.customer.io/v1/api/segments/${segmentId}/membership?limit=${limit}`,
    "json": true,
    "headers": {
      "Authorization": auth
    }
  }).then((UsersToDelete) => {
    console.log(UsersToDelete['ids'].length);
    if (UsersToDelete['ids'].length != 0) {
      UsersToDelete['ids'].forEach((user) => {
        request({
          "method": "DELETE",
          "uri": `https://track.customer.io/api/v1/customers/${user}`,
          "json": true,
          "headers": {
            "Authorization": auth
          }
        }).then(console.log(`deleted user: ${user}`)).catch((err) => {
          console.log(err);
        });
      })
        setTimeout(DeleteData, 1000);
    }
  }).catch((error) => {
    console.log(error);
  });
};

DeleteData();
